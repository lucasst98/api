<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Models\User;


Route::post('register', [AuthController::class,'register']);
Route::post('login', [AuthController::class,'login']);


