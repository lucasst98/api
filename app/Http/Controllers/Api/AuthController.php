<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Registration;
use App\Http\Controllers\Controller;
use App\Models\Register;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\AuthRegisterRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AuthLoginRequest;


class AuthController extends Controller
{
    public function register(AuthRegisterRequest $request)
    {
        $validated = $request->validated();
    
        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
        ]);
        
        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response,201);
    }

    public function login(AuthLoginRequest $request)
    {
        $validated = $request->validated();
        
        if(Auth::attempt($validated)){
            return[
                'message' => 'Usuário logado com sucesso!'
            ];
        }else
            return [
                'message' => 'Email ou senha inválido'
            ];
    }
}